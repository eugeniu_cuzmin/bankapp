package com.example.evgenii.bankapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView withdrawTextView, availableMoney;
    EditText editText;
    float withdrawMoney=0;
    //in case we need to cancel money button press
    int lastAction;
    Account account=new Account(10000);;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //account=
        availableMoney=(TextView)findViewById(R.id.availableTextView);
        withdrawTextView=(TextView)findViewById(R.id.withdrawTextView);
        setAvailableMoneyView();
        editText=(EditText)findViewById(R.id.editText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                withdrawTextView.setText(editText.getText());
                setWthdrowMoney2(Float.parseFloat(editText.getText().toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    public void on10Click(View v)  { omMBClick(10);  }
    public void on50Click(View v)  { omMBClick(50);  }
    public void on100Click(View v){
        omMBClick(100);
    }
    public void on500Click(View v){
        omMBClick(500);
    }

    public void onWithdrawClick(View v){
        if(getWithdrawMoney()%10==0 && getWithdrawMoney()<account.getMoney()) {
            account.setMoney(-getWithdrawMoney());
        }
        else if(getWithdrawMoney()%10!=0){
            Toast toast = Toast.makeText(getApplicationContext(),"The number must end with a 0", Toast.LENGTH_LONG);
            toast.show();
        }
        else {
            Toast toast = Toast.makeText(getApplicationContext(),"You do not have enough money", Toast.LENGTH_LONG);
            toast.show();
        }

        setAvailableMoneyView();
    }

    //cancel last money button click
    public void onReturnClick(View v){
        switch (lastAction){
            case 0: break;
            case 1: omMBClick(-10); lastAction=0; break;
            case 2: omMBClick(-50); lastAction=0; break;
            case 3: omMBClick(-100); lastAction=0; break;
            case 4: omMBClick(-500); lastAction=0; break;
            default: break;
        }
    }

    //change amount of withdrawal money
    private void omMBClick(int i) {
        setWithdrawMoney(i);
        switch (i){
            case 10: lastAction=1; break;
            case 50: lastAction=2; break;
            case 100: lastAction=3; break;
            case 500: lastAction=4; break;
            default: break;
        }
        withdrawTextView.setText(toString());
    }

    public float getWithdrawMoney() {
        return withdrawMoney;
    }

    public void setWithdrawMoney(float money) {
        this.withdrawMoney += money;
    }
    public void setWthdrowMoney2(float money) { this.withdrawMoney = money;    }

    @Override
    public String toString() {
        return "" + withdrawMoney + "";
    }

    //show how much left
    public void setAvailableMoneyView(){
        availableMoney.setText(account.toString());
    }
}
