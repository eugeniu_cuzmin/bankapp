package com.example.evgenii.bankapp;

/**
 * Created by Evgenii on 16.06.2016.
 */
public class Account {
    private float money;

    public Account(float money) {
        this.money = money;
    }

    public Account() {
        this.money = 0;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money += money;
    }

    @Override
    public String toString() {
        return ""  + money + "";
    }
}
